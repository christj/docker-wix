FROM centos:latest
MAINTAINER harbottle <grainger@gmail.com>
ENV wixdir /usr/local/libexec/wix
# Install repos
RUN yum -y -q install http://harbottle.gitlab.io/harbottle-main-release/harbottle-main-release-7.rpm
RUN yum -y -q install epel-release wine32-release
# Install utils
RUN yum -y -q install bsdtar mingw64-gcc wget
# Install wine
RUN yum -y -q install wine.i686 winetricks
# Install .NET (output is very verbose, so dump it)
RUN winetricks -q dotnet40 dotnet_verifier >/dev/null 2>&1
# Install WiX Toolset
RUN mkdir -p $wixdir && wget -qO- https://github.com/wixtoolset/wix3/releases/download/wix3111rtm/wix311-binaries.zip | bsdtar -x -C $wixdir -f-
# Create wrappers
RUN for file in $wixdir/*.exe; do nopath=${file##*/}; echo -e '#!'"/bin/bash\nWINEDEBUG=-all wine32 $file "'$@' > /usr/local/bin/${nopath%.*}; chmod a+x /usr/local/bin/${nopath%.*}; done
RUN mkdir -p /mnt/workspace
WORKDIR /mnt/workspace
