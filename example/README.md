Compiling the `.exe`:

```bash
sudo yum install mingw64-gcc
x86_64-w64-mingw32-gcc ./example.c -o ./example.exe
```

Compiling the `.msi`: see main [README](../README.md).
